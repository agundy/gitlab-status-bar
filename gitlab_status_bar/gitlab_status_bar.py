import os
from datetime import datetime, timedelta
from enum import Enum
import time

from blinkstick import blinkstick
import gitlab


class GitlabStatus(Enum):
    RUNNING  = 'running'
    PENDING  = 'pending'
    SUCCESS  = 'success'
    FAILED   = 'failed'
    CANCELED = 'canceled'
    SKIPPED  = 'skipped'
    CREATED  = 'created'

STATUS_COLOR = {
    GitlabStatus.RUNNING: 'blue',
    GitlabStatus.PENDING: 'yellow',
    GitlabStatus.SUCCESS: 'green',
    GitlabStatus.FAILED: 'red',
    GitlabStatus.CANCELED: 'darkslategray',
    GitlabStatus.SKIPPED: 'lightblue',
    GitlabStatus.CREATED: 'greenyellow'
}


def gitlab_yesterdays_date():
    yesterday = datetime.now() - timedelta(days=1)
    return yesterday.strftime('%Y-%m-%d')


def get_project_ref_pipeline(gl, project_id, ref):
    project = gl.projects.get(project_id)
    pipelines = project.pipelines.list(ref=ref)
    if pipelines:
        return pipelines.pop(0)


def gitlab_grab_latest_project_refs(gl):
    """
    Returns the status of the latest pipelines unique per repository and
    ref
    """
    events = gl.events.list(action_type='pushed', after=gitlab_yesterdays_date())
    print(f'Found {len(events)} events')
    index = 0

    unique_pipelines = dict()
    for event in events:
        pipeline = None
        if 'push_data' in event.attributes:
            project_id = event.project_id
            ref = event.push_data['ref']

            unique_pipelines[(project_id, ref)] = True
    return unique_pipelines.keys()


def render_ci(stick, colors):
    for i, color in enumerate(colors):
        stick.set_color(index=i, name=color)

def setup_blinkstick():
    stick = blinkstick.find_first()
    stick.set_max_rgb_value(40)
    for i in range(9):
        stick.set_color(name='off', index=i)

    return stick

def gitlab_client():
    gitlab_token = os.environ['GITLAB_TOKEN']
    gl = gitlab.Gitlab('https://gitlab.com/', private_token=gitlab_token)
    return gl

def main():
    stick = setup_blinkstick()
    gl = gitlab_client()

    project_refs = []
    count = 0

    while True:
        if count % 3 == 0:
            project_refs = gitlab_grab_latest_project_refs(gl)

        render_statuses = []
        latest_pipelines = []

        for (project_id, ref) in project_refs:
            pipeline = get_project_ref_pipeline(gl, project_id, ref)
            if pipeline:
                latest_pipelines.append(pipeline)

        sorted_pipelines = sorted(latest_pipelines, key=lambda p: p.updated_at)
        for pipeline in sorted_pipelines:
            print(pipeline)
            render_statuses.append(STATUS_COLOR[GitlabStatus(pipeline.status)])

        render_ci(stick, render_statuses[:8])
        time.sleep(10)

if __name__ == "__main__":
    main()
