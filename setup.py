import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gitlab-status-bar",
    version="0.0.1",
    author="Aaron Gunderson",
    author_email="aaron@agundy.com",
    description="Gitlab Status Bar",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/agundy/gitlab-status-bar",
    packages=setuptools.find_packages(),
    classifiers=[],
    install_requires=[
        'blinkstick',
        'python-gitlab',
    ],
    python_requires='>=3.6',
)
